//
//  FileHelpers.m
//  FileBrowser
//
//  Created by Mark Eissler on 8/7/15.
//  Copyright (c) 2015 Mark Eissler. All rights reserved.
//

#import "FileHelpers.h"
#import "Macros.h"

@implementation FileHelpers

+ (BOOL)directoryExists:(NSString *)path
{
  if (!path || path.length == 0) {
    @throw [NSException exceptionWithName:@"NSInvalidArgumentException"
              reason:@"Expected a path, got nil or an empty NSString instead"
              userInfo:nil];
  }
  
  NSString *expandedPath = [FileHelpers resolvePath:path];
  
  NSFileManager *fileManager = [[NSFileManager alloc] init];
  BOOL directoryExists;
  
  [fileManager fileExistsAtPath:expandedPath isDirectory:&directoryExists];
  
  return directoryExists;
}

+ (NSString *)resolvePath:(NSString *)path
{
  if (!path || path.length == 0) {
    @throw [NSException exceptionWithName:@"NSInvalidArgumentException"
              reason:@"Expected a path, got nil or an empty NSString instead"
              userInfo:nil];
  }
  
  NSString *expandedPath = [path stringByStandardizingPath];
  
  return expandedPath;
}

+ (NSString *)localizedTypeForPath:(NSString *)path
{
  if (!path || path.length == 0) {
    return nil;
  }
  
  NSString *uniformTypeId = [FileHelpers uniformTypeForPath:path];
  NSString *localizedType =
    [[NSWorkspace sharedWorkspace] localizedDescriptionForType:uniformTypeId];

  return localizedType;
}

+ (NSString *)localizedTypeForURL:(NSURL *)url
{
  if (!url || url.path.length == 0) {
    return nil;
  }
  
  return [FileHelpers localizedTypeForPath:url.path];
}

+ (NSString *)uniformTypeForPath:(NSString *)path
{
  if (!path || path.length == 0) {
    return nil;
  }
  
  NSString *uniformTypeId =
    [[NSWorkspace sharedWorkspace] typeOfFile:path error:nil];
  
  return uniformTypeId;
}

+ (NSString *)uniformTypeForURL:(NSURL *)url
{
  if (!url || url.path.length == 0) {
    return nil;
  }
  
  return [FileHelpers uniformTypeForPath:url.path];
}

+ (NSImage *)iconForPath:(NSString *)path iconPath:(NSString **)iconPath
{
  NSString *uniformTypeId = [FileHelpers uniformTypeForPath:path];
  NSImage *icon = [[NSWorkspace sharedWorkspace] iconForFileType:uniformTypeId];

  *iconPath = nil;
  
  if (!icon) {
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *defaultIconPath = [bundle pathForResource:@"GenericDocumentIcon" ofType:@"icns"];
    icon = [[NSImage alloc] initWithContentsOfFile:defaultIconPath];
    *iconPath = [defaultIconPath copy];
  }
  
  return icon;
}

+ (NSImage *)iconForURL:(NSURL *)url iconPath:(NSString **)iconPath
{
  if (!url || url.path.length == 0) {
    return nil;
  }
  
  return [FileHelpers iconForPath:url.path iconPath:iconPath];
}

@end
