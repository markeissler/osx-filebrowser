//
//  FileHelpers.h
//  FileBrowser
//
//  Created by Mark Eissler on 8/7/15.
//  Copyright (c) 2015 Mark Eissler. All rights reserved.
//

#import <AppKit/AppKit.h>

/**
 *  A collection of file system helper (i.e. convenience) methods.
 */
@interface FileHelpers : NSObject

#pragma mark - Class methods

/**
 *  @brief Verifies that path exists and is a directory.
 *
 *  @param path Path to verify.
 *
 *  @return YES if directory exists, otherwise NO.
 *
 *  @throws "<NSInvalidArgumentException>" When path is nil or empty.
 */
+ (BOOL)directoryExists:(NSString *)path;

/**
 *  @brief Resolves a path by interpreting tokens and removing extraneous
 *  components.
 *
 *  This method attempts to create a valid full path to a resource by:
 *    - interpreting a leading tilde to a user directory
 *    - stripping redundant references to parent directories
 *    - resolving symlinks
 *    .
 *
 *  @param path Path to resolve.
 *
 *  @return Resolved path on success, nil on failure.
 *
 *  @throws "<NSInvalidArgumentException>" When path is nil or empty.
 */
+ (NSString *)resolvePath:(NSString *)path;

/**
 *  @brief Returns a localized type description for resource at path.
 *
 *  @param path Path to resource.
 *
 *  @return Localized resource type on success, nil on failure.
 *
 *  @see #localizedTypeForURL:
 */
+ (NSString *)localizedTypeForPath:(NSString *)path;

/**
 *  @brief Returns a localized type description for resource at url.
 *
 *  @param url NSURL for resource.
 *
 *  @return Localized resource type on success, nil on failure.
 *
 *  @note This is a convenience method that calls #localizedTypeForPath.
 *
 *  @see #localizedTypeForPath:
 */
+ (NSString *)localizedTypeForURL:(NSURL *)url;

/**
 *  @brief Returns a uniform type identifier for resource at path.
 *
 *  @param path Path to resource.
 *
 *  @return Uniform type identifier on success, nil on failure.
 */
+ (NSString *)uniformTypeForPath:(NSString *)path;

/**
 *  @brief Returns a uniform type identifier for resource at url.
 *
 *  @param url NSURL for resource.
 *
 *  @return Uniform type identifier on success, nil on failure.
 */
+ (NSString *)uniformTypeForURL:(NSURL *)url;

/**
 *  @brief Returns an icon for resource at path.
 *
 *  The icon is the system registered icon for the resource type based on its
 *  uniform type identifier. If no icon is available, a generic icon will be
 *  returned. The iconPath will contain an NSString pointing to a path only if
 *  a generic icon is returned.
 *
 *  @param path     Path to resource.
 *  @param iconPath A by-reference parameter containing an NSString if an icon
 *  path is available, otherwise nil.
 *
 *  @return Icon image on success, nil on failure.
 *
 *  @see #iconForURL:iconPath:
 */
+ (NSImage *)iconForPath:(NSString *)path iconPath:(NSString **)iconPath;

/**
 *  @brief Returns an icon for resource at url.
 *
 *  The icon is the system registered icon for the resource type based on its
 *  uniform type identifier. If no icon is available, a generic icon will be
 *  returned. The iconPath will contain an NSString pointing to a path only if
 *  a generic icon is returned.
 *
 *  @param path     NSURL for resource.
 *  @param iconPath A by-reference parameter containing an NSString if an icon
 *  path is available, otherwise nil.
 *
 *  @return Icon image on success, nil on failure.
 *
 *  @see #iconForPath:iconPath:
 */
+ (NSImage *)iconForURL:(NSURL *)url iconPath:(NSString **)iconPath;

#pragma mark - Lifecycle
#pragma mark - Custom Accessors
#pragma mark - IBActions
#pragma mark - Public

@end
