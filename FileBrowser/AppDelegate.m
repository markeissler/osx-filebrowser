//
//  AppDelegate.m
//  FileBrowser
//
//  Created by Mark Eissler on 8/5/15.
//  Copyright (c) 2015 Mark Eissler. All rights reserved.
//

#import "AppDelegate.h"
#import "AppController.h"
#import "DirectoryBrowseViewController.h"
#import "Helpers/Konstants.h"
#import "Helpers/Macros.h"

@interface AppDelegate ()

@property (strong) IBOutlet AppController *appController;

// menus
@property (weak) IBOutlet NSMenu *fileMenu;
@property (weak) IBOutlet NSMenu *viewMenu;
@property (weak) IBOutlet NSMenuItem *viewRefreshMenuItem;
@property (weak) IBOutlet NSMenu *browseMenu;
@property (weak) IBOutlet NSMenuItem *browseFileOpenMenuItem;
@property (weak) IBOutlet NSMenuItem *browseFileShowInFinderMenuItem;
@property (weak) IBOutlet NSMenuItem *browseFileCopyPathMenuItem;
@property (weak) IBOutlet NSMenuItem *browseFileDetailsMenuItem;

// private methods

// notifications
- (void)browseViewDidClose:(NSNotification *)notification;
- (void)browseViewDidOpen:(NSNotification *)notification;
- (void)browseViewDidEmpty:(NSNotification *)notification;
- (void)browseViewDidPopulate:(NSNotification *)notification;
- (void)browseSelectionDidChange:(NSNotification *)notification;

// file menu
- (IBAction)newWindow:(id)sender;

// view menu
- (IBAction)refreshView:(id)sender;

// browse menu
- (IBAction)openSelectedFile:(id)sender;
- (IBAction)showSelecteFileInFinder:(id)sender;
- (IBAction)copySelectedFilePath:(id)sender;
- (IBAction)showSelectedFileDetails:(id)sender;

@end

@implementation AppDelegate

#pragma mark - Class Methods

#pragma mark - Lifecycle

- (void)dealloc
{
  NSNotificationCenter *notificationCenter =
    [NSNotificationCenter defaultCenter];
  [notificationCenter removeObserver:self];
}

- (void)awakeFromNib
{
  // register to receive directory browse controller notifications
  NSNotificationCenter *notificationCenter =
    [NSNotificationCenter defaultCenter];
  [notificationCenter addObserver:self
     selector:@selector(browseViewDidClose:)
     name:kMIX_DirectoryBrowseViewControllerViewDidClose
     object:nil];
  
  [notificationCenter addObserver:self
     selector:@selector(browseViewDidOpen:)
     name:kMIX_DirectoryBrowseViewControllerViewDidOpen
     object:nil];
  
  [notificationCenter addObserver:self
     selector:@selector(browseViewDidEmpty:)
     name:kMIX_DirectoryBrowseViewControllerViewDidEmpty
     object:nil];

  [notificationCenter addObserver:self
     selector:@selector(browseViewDidPopulate:)
     name:kMIX_DirectoryBrowseViewControllerViewDidPopulate
     object:nil];

  [notificationCenter addObserver:self
     selector:@selector(browseSelectionDidChange:)
     name:kMIX_DirectoryBrowseViewControllerSelectionDidChange
     object:nil];

  self.fileMenu.autoenablesItems = YES;
  
  self.viewMenu.autoenablesItems = NO;
  self.viewRefreshMenuItem.enabled = NO;
  
  self.browseMenu.autoenablesItems = NO;
  self.browseFileCopyPathMenuItem.enabled = NO;
}

#pragma mark - Custom Accessors

#pragma mark - IBActions

- (IBAction)newWindow:(id)sender
{
  [self.appController showWindow:self];
}

- (IBAction)refreshView:(id)sender
{
  [self.appController
     performSelector:@selector(directoryBrowserViewRefresh:)
     withObject:self
     afterDelay:0.0];
}

- (IBAction)openSelectedFile:(id)sender
{
  [self.appController
     performSelector:@selector(directoryBrowserViewOpenSelectedFile:)
     withObject:self
     afterDelay:0.0];
}

- (IBAction)showSelecteFileInFinder:(id)sender
{
  [self.appController
     performSelector:@selector(directoryBrowserViewShowSelectedFileInFinder:)
     withObject:self
     afterDelay:0.0];
}

- (IBAction)copySelectedFilePath:(id)sender
{
  [self.appController
     performSelector:@selector(directoryBrowserViewCopySelectedFilePath:)
     withObject:self
     afterDelay:0.0];
}

- (IBAction)showSelectedFileDetails:(id)sender
{
  [self.appController
     performSelector:@selector(directoryBrowserViewShowSelectedFileDetails:)
     withObject:self
     afterDelay:0.0];
}

#pragma mark - Notification handlers

- (void)browseViewDidClose:(NSNotification *)notification
{
  self.viewRefreshMenuItem.enabled = NO;
}

- (void)browseViewDidOpen:(NSNotification *)notification
{
  self.viewRefreshMenuItem.enabled = YES;
}

- (void)browseViewDidEmpty:(NSNotification *)notification
{
  self.viewRefreshMenuItem.enabled = YES;
}

- (void)browseViewDidPopulate:(NSNotification *)notification
{
  self.viewRefreshMenuItem.enabled = YES;
}

- (void)browseSelectionDidChange:(NSNotification *)notification
{
  NSUInteger itemCount = [[[notification userInfo] objectForKey:@"itemCount"] unsignedIntegerValue];
  
  if (itemCount == 1) {
    self.browseFileOpenMenuItem.enabled = YES;
    self.browseFileShowInFinderMenuItem.enabled = YES;
    self.browseFileCopyPathMenuItem.enabled = YES;
    self.browseFileDetailsMenuItem.enabled = YES;
  } else {
    self.browseFileOpenMenuItem.enabled = NO;
    self.browseFileShowInFinderMenuItem.enabled = NO;
    self.browseFileCopyPathMenuItem.enabled = NO;
    self.browseFileDetailsMenuItem.enabled = NO;
  }
}

#pragma mark - Public
#pragma mark - Private

#pragma mark - Protocol conformance (NSMenuValidation)

- (BOOL)validateMenuItem:(NSMenuItem *)menuItem
{
  SEL action = [menuItem action];
  
  if (action == @selector(newWindow:)) {
    if (self.appController.window.isVisible) {
      return NO;
    }
  }
  
  return YES;
}

#pragma mark - ApplicationDelegate

- (void)applicationWillFinishLaunching:(NSNotification *)notification
{
}

- (void)applicationDidFinishLaunching:(NSNotification *)notification
{
  if (!_appController)
  {
    _appController = [[AppController alloc] init];
  }
  
  [self.appController showWindow:self];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification
{
}

#pragma mark - XyzDelegate (insert)
#pragma mark - NSCopying
#pragma mark - NSObject
#pragma mark - ...

@end
