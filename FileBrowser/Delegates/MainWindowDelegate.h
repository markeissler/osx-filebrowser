//
//  MainWindowDelegate.h
//  FileBrowser
//
//  Created by Mark Eissler on 8/6/15.
//  Copyright (c) 2015 Mark Eissler. All rights reserved.
//

#import <AppKit/AppKit.h>

@interface MainWindowDelegate : NSObject <NSWindowDelegate>

@end
