//
//  MainWindowDelegate.m
//  FileBrowser
//
//  Created by Mark Eissler on 8/6/15.
//  Copyright (c) 2015 Mark Eissler. All rights reserved.
//

#import "MainWindowDelegate.h"
#import "DirectorySelectViewController.h"
#import "DirectoryBrowseViewController.h"
#import "Macros.h"

@implementation MainWindowDelegate

- (BOOL)windowShouldZoom:(NSWindow *)window toFrame:(NSRect)newFrame
{
  return YES;
}

@end
