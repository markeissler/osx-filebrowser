//
//  MainView.m
//  FileBrowser
//
//  Created by Mark Eissler on 8/6/15.
//  Copyright (c) 2015 Mark Eissler. All rights reserved.
//

#import "MainView.h"
#import "Konstants.h"
#import "Macros.h"

@interface MainView ()

@property (strong) NSView *contentView;

@end

@implementation MainView

@synthesize contentView = _contentView;

#pragma mark - Class Methods

#pragma mark - Lifecycle

- (void)awakeFromNib
{
#if DEBUG_LAYOUT
  self.wantsLayer = YES;
  self.layer.borderWidth = 1;
#endif
  
  NSRect parentViewRect = [self frame];
  _contentView = [[NSView alloc] initWithFrame:parentViewRect];
  [self addSubview:_contentView];

#if DEBUG_LAYOUT
  LOG_PROGRESS(@"superView: ", [NSString stringWithFormat:@"width: %f, height; %f",
                                    self.superview.frame.size.width,
                                    self.superview.frame.size.height]);
#endif
}

- (instancetype)init
{
  self = [super init];

  if (self) {
    NSRect parentViewRect = [self frame];
    _contentView = [[NSView alloc] initWithFrame:parentViewRect];
    [self addSubview:_contentView];
  }

  return self;
}

- (void)drawRect:(NSRect)dirtyRect {
  [super drawRect:dirtyRect];
  
  // Drawing code here.
}

#pragma mark - Custom Accessors

- (NSView *)contentView
{
  return _contentView;
}

- (void)setContentView:(NSView *)view
{
  if (view != nil && [view superview] == nil)
  {
    [_contentView removeFromSuperview];
    _contentView = view;
    [self setFrame:_contentView.frame];
    [self addSubview:_contentView];

    // set auto-layout constraints, this view should stick to its superview
    NSDictionary *viewNameMap = @{
      @"contentView" : _contentView
    };
    NSArray *horizontalConstraints =
      [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[contentView]-0-|"
         options:0 metrics:nil views:viewNameMap];
    NSArray *verticalConstraints =
      [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[contentView]-0-|"
         options:0 metrics:nil views:viewNameMap];

    [self addConstraints:horizontalConstraints];
    [self addConstraints:verticalConstraints];

#if DEBUG_LAYOUT
    _contentView.wantsLayer = YES;
    _contentView.layer.borderWidth = 1;
    _contentView.layer.borderColor = [[NSColor orangeColor] CGColor];
#endif

    // update auto-layout
    [self invalidateIntrinsicContentSize];
    [self layoutSubtreeIfNeeded];

#if DEBUG_LAYOUT
    LOG_PROGRESS(@"contentView: ", [NSString stringWithFormat:@"width: %f, height; %f",
                                      _contentView.frame.size.width,
                                      _contentView.frame.size.height]);
#endif
  }
}

#pragma mark - IBActions
#pragma mark - Public
#pragma mark - Private
#pragma mark - Protocol conformance
#pragma mark - XyzDelegate (insert)
#pragma mark - NSCopying
#pragma mark - NSObject

#pragma mark - NSView

- (NSSize)intrinsicContentSize
{
  return self.contentView.frame.size;
}

#pragma mark - ...

@end
