//
//  MainView.h
//  FileBrowser
//
//  Created by Mark Eissler on 8/6/15.
//  Copyright (c) 2015 Mark Eissler. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface MainView : NSView

#pragma mark - Class methods
#pragma mark - Lifecycle

#pragma mark - Custom Accessors

- (NSView *)contentView;
- (void)setContentView:(NSView *)view;

#pragma mark - IBActions
#pragma mark - Public

@end
