//
//  MIXTableViewDelegate.h
//  FileBrowser
//
//  Created by Mark Eissler on 8/14/15.
//  Copyright (c) 2015 Mark Eissler. All rights reserved.
//

#ifndef FileBrowser_MIXTableViewDelegate_h
#define FileBrowser_MIXTableViewDelegate_h

@class MIXTableView;

/**
 *  @protocol MIXTableViewDelegate
 *
 *  The MIXTableViewDelegate protocol groups methods that support an instance
 *  of MIXTableViewDelegate. Delegate objects will be registered to receive
 *  notifications for all delegate methods implemented.
 */
@protocol MIXTableViewDelegate <NSTableViewDelegate>

/**
 *  @brief Allows a delegate to override a context menu display request.
 *
 *  Invoked on the delegate immediately before the MIXTableView object's
 *  context menu is displayed. This gives the delegate a chance to override
 *  whether a menu is displayed. In addition, an alternate menu can be
 *  provided by setting the altMenu parameter.
 *
 *  If the delegate does not implement this method the default behavior is that
 *  the context menu will be displayed.
 *
 *  @param sender  The object that initiated the action.
 *  @param menu    The NSMenu that will be displayed.
 *  @param altMenu A by-reference parameter containing an alternate menu to
 *  display, otherwise nil.
 *
 *  @return Reference YES if the table view should display the context menu, NO
 *  otherwise.
 */
- (BOOL)tableView:(MIXTableView *)tableView
  shouldOpenContextMenu:(NSMenu *)menu alternateMenu:(NSMenu **)altMenu;

#pragma mark - Optional

@optional

/**
 *  @brief 
 *
 *  Invoked on the delegate when the #kMIX_TableViewWillOpenContextMenu
 *  notification is sent.
 *
 *  The value of the userInfo key <b>event</b> will contain a reference to the
 *  mouse-down NSEvent object that triggered context menu display; the value of
 *  the userInfo key <b>menu</b> will contain a reference to the NSMenu object
 *  that will be shown.
 *
 *  @param notification A notification named kMIX_TableViewWillOpenContextMenu.
 */
- (void)tableViewWillOpenContextMenu:(NSNotification *)notification;

/**
 *  @brief 
 *
 *  Invoked on the delegate when the #kMIX_TableViewDidOpenContextMenu
 *  notification is sent.
 *
 *  The value of the userInfo key <b>event</b> will contain a reference to the
 *  mouse-down NSEvent object that triggered context menu display; the value of
 *  the userInfo key <b>menu</b> will contain a reference to the NSMenu object
 *  that was shown.
 *
 *  @param notification A notification named kMIX_TableViewDidOpenContextMenu.
 */
- (void)tableViewDidOpenContextMenu:(NSNotification *)notification;

@end


#endif
