//
//  MIXTableView.h
//  FileBrowser
//
//  Created by Mark Eissler on 8/14/15.
//  Copyright (c) 2015 Mark Eissler. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "MIXTableViewDelegate.h"

#pragma mark - Konstants (extern)

/**
 *  @brief Posted when the MIXTableView object's context menu is about to be
 *  displayed.
 *
 *  The notification object is the MIXTableView object whose context menu is
 *  about to be displayed. This notification contains a userInfo dictionary with
 *  the following keys:
 *    - event (mouse-down event object that triggered context menu display)
 *    - menu (NSMenu object that will be shown)
 *    .
 */
extern NSString * const kMIX_TableViewWillOpenContextMenu;

/**
 *  @brief Posted when the MIXTableView object's context menu has been
 *  displayed.
 *
 *  The notification object is the MIXTableView object whose context menu has
 *  been displayed. This notification contains a userInfo dictionary with
 *  the following keys:
 *    - event (mouse-down event object that triggered context menu display)
 *    - menu (NSMenu object that was shown)
 *    .
 */
extern NSString * const kMIX_TableViewDidOpenContextMenu;

@interface MIXTableView : NSTableView <MIXTableViewDelegate>

#pragma mark - Class methods
#pragma mark - Lifecycle
#pragma mark - Custom Accessors
#pragma mark - IBActions
#pragma mark - Public

@end
