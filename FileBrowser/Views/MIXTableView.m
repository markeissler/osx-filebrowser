//
//  MIXTableView.m
//  FileBrowser
//
//  Created by Mark Eissler on 8/14/15.
//  Copyright (c) 2015 Mark Eissler. All rights reserved.
//

#import "MIXTableView.h"
#import "Macros.h"
#include <dlfcn.h>
#pragma mark - Konstants

//
// notification messages
//
NSString * const kMIX_TableViewWillOpenContextMenu =
  @"kMIX_TableViewWillOpenContextMenu";
NSString * const kMIX_TableViewDidOpenContextMenu =
  @"kMIX_TableViewDidOpenContextMenu";

@interface MIXTableView () {
  @private
  __strong NSObject *_lockObj;
  __strong NSMenu *_emptyMenu;
}
// private methods
- (void)initCommon;
@end

@implementation MIXTableView

#pragma mark - Class Methods

#pragma mark - Lifecycle

- (void)initCommon
{
  if (self)
  {
    _lockObj = [[NSObject alloc] init];
    _emptyMenu = [[NSMenu alloc] init];
  } else {
    @throw [NSException exceptionWithName:@"IllegalStateException"
              reason:[NSString stringWithFormat:@"Can't call initCommon on nil object for class: %@",
                        [self class]]
              userInfo:nil];
  }

  return;
}

- (instancetype)init
{
  self = [super init];
  
  if (self)
  {
    [self initCommon];
  }
  
  return self;
}

- (instancetype)initWithFrame:(NSRect)frameRect
{
  self = [super initWithFrame:frameRect];
  
  if (self)
  {
    [self initCommon];
  }
  
  return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
  self = [super initWithCoder:coder];
  
  if (self)
  {
    [self initCommon];
  }
  
  return self;
}

- (void)dealloc
{
  if (_delegate) {
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter removeObserver:_delegate name:nil object:self];
  }
}

- (void)drawRect:(NSRect)dirtyRect {
  [super drawRect:dirtyRect];
}

#pragma mark - Custom Accessors
#pragma mark - IBActions
#pragma mark - Notification handlers
#pragma mark - Public
#pragma mark - Private

#pragma mark - Protocol conformance (MIXTableViewDelegate)

- (BOOL)tableView:(MIXTableView *)tableView shouldOpenContextMenu:(NSMenu *)menu
  alternateMenu:(NSMenu *__autoreleasing *)altMenu
{
  return YES;
}

#pragma mark - XyzDelegate (insert)
#pragma mark - NSCopying
#pragma mark - NSObject

#pragma mark - NSTableView

- (id<MIXTableViewDelegate>)delegate
{
  @synchronized(_lockObj) {
    return (id<MIXTableViewDelegate>)_delegate;
  }
}

- (void)setDelegate:(id<MIXTableViewDelegate>)delegate
{
  @synchronized(_lockObj)
  {
    NSNotificationCenter *notificationCenter =
      [NSNotificationCenter defaultCenter];

    if (_delegate)
    {
      if (_delegate == delegate)
      {
        return;
      }
      [notificationCenter removeObserver:_delegate name:nil object:self];
    }

    if (delegate)
    {
      super.delegate = delegate;
    } else {
      super.delegate = self;
    }

    SEL selector = nil;

    // kMIX_TableViewWillOpenContextMenu
    selector = @selector(tableViewWillOpenContextMenu:);
    if ([_delegate respondsToSelector:selector])
    {
      [notificationCenter addObserver:_delegate
         selector:selector
         name:kMIX_TableViewWillOpenContextMenu
         object:self];
    }
  }
}

- (NSMenu *)menuForEvent:(NSEvent *)event
{
  BOOL openContextMenu = YES;
  NSMenu *menu = [super menuForEvent:event];
  NSMenu *altMenu;

  if (self.delegate &&
    [self.delegate
       respondsToSelector:@selector(tableView:shouldOpenContextMenu:alternateMenu:)])
  {
    openContextMenu = [(id<MIXTableViewDelegate>)self.delegate
                         tableView:self shouldOpenContextMenu:menu
                         alternateMenu:&altMenu];
  }

  if (!openContextMenu)
  {
    // Warning: Returning nil here should work to prevent any menu from being
    // displayed but instead, there is a side effect that seems to remove the
    // menu all together.
    return _emptyMenu;
  }

  if (altMenu)
  {
    menu = altMenu;
  }

  NSNotificationCenter *notificationCenter =
    [NSNotificationCenter defaultCenter];
  NSDictionary *userInfo = @{
    @"event": [event copy],
    @"menu": menu
  };
  [notificationCenter postNotificationName:kMIX_TableViewWillOpenContextMenu
     object:self
     userInfo:userInfo];

  return menu;
}

#pragma mark - ...

@end
