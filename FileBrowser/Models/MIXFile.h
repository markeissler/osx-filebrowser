//
//  MIXFile.h
//  FileBrowser
//
//  Created by Mark Eissler on 8/5/15.
//  Copyright (c) 2015 Mark Eissler. All rights reserved.
//

#import <Cocoa/Cocoa.h>

/**
 *  @brief File meta information class.
 *
 *  The MIXFile meta class is a container for various commonly used attributes
 *  associated with a file object including:
 *    - name
 *    - type
 *    - icon
 *    - size
 *    - prettySize (an NSStirng containing integer plus units)
 *    .
 */
@interface MIXFile : NSObject <NSCopying>

/**
 *  @brief The file path.
 *
 *  The file path will be unescaped using 
 *  NSString+stringByReplacingPercentEscapesUsingEncoding:.
 *
 *  @throws "<NSInvalidArgumentException>" When path is nil or empty.
 */
@property (readwrite, copy) NSString *path;

/**
 *  @brief The file url.
 *
 *  The file path converted into an NSURL.
 *
 *  @see #path
 */
@property (readonly, copy) NSURL *url;

/**
 *  @brief The file name component including the extension (if present).
 */
@property (readonly, copy) NSString *name;

/**
 *  @brief The file type.
 *
 *  The file type is the extension, minus the dot. If there is no extension,
 *  then the file type will be nil.
 */
@property (readonly, copy) NSString *type;

/**
 *  @brief The localized file type string.
 *
 *  If the file type cannot be localized, the prettyType will be nil.
 */
@property (readonly, copy) NSString *prettyType;

/**
 *  @brief The registered system file icon for the file.
 */
@property (readonly, strong) NSImage *icon;

/**
 *  @brief The file icon path.
 *
 *  The icon path will be nil unless a generic icon has been returned.
 */
@property (readonly, copy) NSString *iconPath;

/**
 *  @brief The file size in bytes.
 */
@property (readonly, assign) NSUInteger size;

/**
 *  @brief The file size including units.
 *
 *  The file units will be represented as one of the following:
 *    - bytes
 *    - KB
 *    - MB
 *    - GB
 *    - TB
 *    - PB
 *    - EB
 *    - ZB
 *    .
 */
@property (readonly, copy) NSString *prettySize;

/**
 *  @brief The created date.
 */
@property (readonly, copy) NSDate *created;

/**
 *  @brief The localized created date.
 */
@property (readonly, copy) NSString *prettyCreated;

/**
 *  @brief The modified date.
 */
@property (readonly, copy) NSDate *modified;

/**
 *  @brief The localized modified date.
 */
@property (readonly, copy) NSString *prettyModified;

#pragma mark - Class methods

/**
 *  @brief Returns an initialized MIXFile object for path.
 *
 *  @param path Full path to file.
 *
 *  @return Initialized MIXFile object on success, nil on failure.
 *
 *  @see #initWithPath:
 */
+ (instancetype)fileWithPath:(NSString *)path;

/**
 *  @brief Returns an initialized MIXFile object for url.
 *
 *  @param url NSURL for file.
 *
 *  @return Initialized MIXFile object on success, nil on failure.
 *
 *  @see #initWithURL:
 */
+ (instancetype)fileWithURL:(NSURL *)url;

#pragma mark - Lifecycle

/**
 *  @brief Initializes a new MIXFile object for path.
 *
 *  @param path Full path to file.
 *
 *  @return Initialized MIXFile object on success, nil on failure.
 *
 *  @note This is a convenience method that calls #initWithURL.
 *
 *  @throws "<NSInvalidArgumentException>" When path is nil or empty.
 *
 *  @see #initWithURL:
 */
- (instancetype)initWithPath:(NSString *)path;

/**
 *  @brief Initializes a new MIXFile object for url.
 *
 *  The url must be fully initialized, for example, by calling the
 *  NSURL#fileURLWithPath: method.
 *
 *  @param url NSURL for file.
 *
 *  @return Initialized MIXFile object on success, nil on failure.
 *
 *  @throws "<NSInvalidArgumentException>" When url is nil or partially
 *  initialized.
 *
 *  @remark Designated initializer.
 */
- (instancetype)initWithURL:(NSURL *)url;

#pragma mark - Custom Accessors
#pragma mark - IBActions

#pragma mark - Public

/**
 *  @brief Determines whether the receiver and a given MIXFile object are
 *  equal.
 *
 *  The objects are compared against the following properties:
 *    - path
 *    - name
 *    - type
 *    - size
 *    - prettySize
 *    - iconPath
 *    .
 *
 *  @param file MIXFile to compare this object to.
 *
 *  @return YES if equal, otherise NO.
 */
- (BOOL)isEqualToMIXFile:(MIXFile *)file;

/**
 *  @brief Resolve the MIXFile path.
 *
 *  The path is stored internally unresolved (leading tilde in path is not
 *  expanded, and symlinks are not resolved). This is done so that the original
 *  raw path is available later, provided that the object was initialized with
 *  a path (#initWithPath) instead of url (#initWithUrl). This method returns
 *  the path with both tilde expanded and symlinks resolved.
 *
 *  @return String containing path on success, nil on failure.
 */
- (NSString *)resolvedPath;

@end
