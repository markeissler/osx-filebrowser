//
//  MIXFile.m
//  FileBrowser
//
//  Created by Mark Eissler on 8/5/15.
//  Copyright (c) 2015 Mark Eissler. All rights reserved.
//

#import "MIXFile.h"
#import "FileHelpers.h"
#import "Macros.h"

@interface MIXFile ()

@property (readwrite, copy) NSString *name;
@property (readwrite, copy) NSString *type;
@property (readwrite, copy) NSString *prettyType;
@property (readwrite, strong) NSImage *icon;
@property (readwrite, copy) NSString *iconPath;
@property (readwrite, assign) NSUInteger size;
@property (readwrite, copy) NSString *prettySize;
@property (readwrite, copy) NSDate *created;
@property (readwrite, copy) NSString *prettyCreated;
@property (readwrite, copy) NSDate *modified;
@property (readwrite, copy) NSString *prettyModified;

/**
 *  @internal
 *
 *  Private init method returns an initialized but empty object.
 *
 *  @return Initalized object on success, nil on failure.
 */
- (instancetype)init_p;

@end

@implementation MIXFile

@synthesize url = _url;

#pragma mark - Class Methods

+ (instancetype)fileWithPath:(NSString *)path
{
  MIXFile *file = [[self alloc] initWithPath:path];

  return file;
}

+ (instancetype)fileWithURL:(NSURL *)url
{
  MIXFile *file = [[self alloc] initWithURL:url];

  return file;
}

#pragma mark - Lifecycle

- (instancetype)init
{
  @throw [NSException exceptionWithName:@"WrongInitializer"
                                 reason:[NSString stringWithFormat:@"Call - [%@ initWithURL or initWithPath]",
                                         [self class]]
                               userInfo:nil];
  
  return nil;
}

- (instancetype)initWithPath:(NSString *)path
{
  if (!path || path.length == 0) {
    @throw [NSException exceptionWithName:@"NSInvalidArgumentException"
              reason:@"Expected a path, got nil or an empty NSString instead"
              userInfo:nil];
  }

  NSString *resolvedPath = [FileHelpers resolvePath:path];
  NSURL *url = [NSURL fileURLWithPath:resolvedPath];
  
  return [self initWithURL:url];
}

- (instancetype)initWithURL:(NSURL *)url
{
  if (!url || url.path.length == 0) {
    @throw [NSException exceptionWithName:@"NSInvalidArgumentException"
              reason:@"Expected an url, got nil or a partially initialized url instead"
              userInfo:nil];
  }

  self = [super init];
  
  if (self) {
    NSDictionary *attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:[url path] error:nil];
    
    _url = url;
    
    _path = [[url path] copy];
    _name = [[url lastPathComponent] copy];
    
    _type = [[url pathExtension] copy];
    _prettyType = [[FileHelpers localizedTypeForPath:_path] copy];

    _size = [attributes fileSize];
    
    _prettySize = [[NSByteCountFormatter stringFromByteCount:_size
       countStyle:NSByteCountFormatterCountStyleFile] copy];
    
    _created = [[attributes fileCreationDate] copy];
    _modified = [[attributes fileModificationDate] copy];
    
    // date format: Wed, Aug 12, 13:54
    NSString *formatString = [NSDateFormatter dateFormatFromTemplate:@"EdMMM, HH:mm" options:0
                                          locale:[NSLocale currentLocale]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:formatString];
    
    _prettyCreated = [[dateFormatter stringFromDate:_created] copy];
    _prettyModified = [[dateFormatter stringFromDate:_modified] copy];

    NSString *iconPathPtr = nil;
    _icon = [FileHelpers iconForPath:_path iconPath:&iconPathPtr];
    if (iconPathPtr) {
      _iconPath = [iconPathPtr copy];
    }
  }
  
  return self;
}

- (instancetype)init_p
{  
  return [super init];
}

#pragma mark - Custom Accessors

- (NSURL *)url
{
  return _url;
}

#pragma mark - IBActions
#pragma mark - Notification handlers

#pragma mark - Public

- (BOOL)isEqualToMIXFile:(MIXFile *)file
{
  if (!file) {
    return NO;
  }
  
  // evaluate all other conditions
  if (self.path != file.path)
  {
    return NO;
  }
  
  if (![self.name isEqualToString:file.name])
  {
    return NO;
  }
  
  if (self.type != file.type) {
    return NO;
  }
  
  if (self.prettyType != file.prettyType) {
    return NO;
  }
  
  if (self.size != file.size) {
    return NO;
  }
  
  if (self.prettySize != file.prettySize) {
    return NO;
  }
  
  // we don't compare that actual icon NSImage, just the path!
  if (self.iconPath != file.iconPath) {
    return NO;
  }

  return YES;
}

- (NSString *)resolvedPath
{
  return [FileHelpers resolvePath:self.path];
}

#pragma mark - Private
#pragma mark - Protocol conformance
#pragma mark - XyzDelegate (insert)

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone
{
  id file =
    [[[self class] allocWithZone:zone] init_p];

  if (file)
  {
    ((MIXFile *)file)->_path = [_path copy];
    ((MIXFile *)file)->_name = [_name copy];
    ((MIXFile *)file)->_type = [_type copy];
    ((MIXFile *)file)->_prettyType = [_prettyType copy];
    ((MIXFile *)file)->_icon = [_icon copy];
    ((MIXFile *)file)->_iconPath = [_iconPath copy];
    ((MIXFile *)file)->_size = _size;
    ((MIXFile *)file)->_prettySize = [_prettySize copy];
  }

  return file;
}

#pragma mark - NSObject

- (NSString *)description
{
  NSMutableString *output = [NSMutableString string];

  [output appendFormat:@"%@: (path: %@, type: %@, prettyType: %@",
     self.name,
     self.path,
     self.type,
     self.prettyType];
  [output appendFormat:@", size: %ld, prettySize: %@", self.size,
     self.prettySize];
  [output appendFormat:@", iconPath: %@)", self.iconPath];

  return output;
}

- (NSString *)debugDescription
{
  NSMutableString *output = [NSMutableString string];
  NSString *delimiter = @"-----------------------------------------------";
  
  [output appendFormat:@"%@\nProperties for object: %@\n>\n", delimiter, [self class]];
  [output appendFormat:@"name: %@\n", self.name];
  [output appendFormat:@"path: %@\n", self.path];
  [output appendFormat:@"type: %@ (%@)\n", self.type, self.prettyType];
  [output appendFormat:@"size: %ld (%@)\n", self.size, self.prettySize];
  [output appendFormat:@"icon: %@\n", self.iconPath];
  
  [output appendFormat:@"<\n%@", delimiter];
  
  return output;
}

- (id)debugQuickLookObject
{
  return [self debugDescription];
}

- (BOOL)isEqual:(id)other
{
  if (other == self)
  {
    return YES;
  }
  
  if (!other || ![other isKindOfClass:[MIXFile class]]) {
    return NO;
  }
  
  return [self isEqualToMIXFile:other];
}

- (NSUInteger)hash {
  NSUInteger result = 1;
  NSUInteger prime = 31;
  
  // Add the class name (subclasses are not equal to their superclass)
  result = prime * result + [[self class] hash];

  // Add any object that already has a hash function (NSString)
  result = prime * result + [self.path hash];
  result = prime * result + [self.name hash];
  result = prime * result + [self.type hash];
  result = prime * result + [self.prettyType hash];
  result = prime * result + [self.prettySize hash];
  result = prime * result + [self.iconPath hash];

  // Add primitive variables (int)
  result = prime * result + self.size;

  return result;
}

#pragma mark - ...

@end
