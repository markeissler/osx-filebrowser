//
//  FileDetailsPanelController.m
//  FileBrowser
//
//  Created by Mark Eissler on 8/13/15.
//  Copyright (c) 2015 Mark Eissler. All rights reserved.
//

#import "FileDetailsPanelController.h"
#import "MIXFile.h"
#import "Macros.h"

@interface FileDetailsPanelController ()

@property (weak) IBOutlet NSImageView *iconPreview;
@property (weak) IBOutlet NSTextField *titleNameTextField;
@property (weak) IBOutlet NSTextField *titleModifiedTextFieldLabel;
@property (weak) IBOutlet NSTextField *titleModifiedTextField;
@property (weak) IBOutlet NSTextField *pathTextFieldLabel;
@property (weak) IBOutlet NSScrollView *pathScrollView;
@property (weak) IBOutlet NSTextField *kindTextFieldLabel;
@property (weak) IBOutlet NSTextField *kindTextField;
@property (weak) IBOutlet NSTextField *sizeTextFieldLabel;
@property (weak) IBOutlet NSTextField *sizeTextField;
@property (weak) IBOutlet NSTextField *createdTextFieldLabel;
@property (weak) IBOutlet NSTextField *createdTextField;
@property (weak) IBOutlet NSTextField *modifiedTextFieldLabel;
@property (weak) IBOutlet NSTextField *modifiedTextField;

@end

@implementation FileDetailsPanelController

#pragma mark - Class Methods

#pragma mark - Lifecycle

- (instancetype)init
{
  self = [super initWithWindowNibName:@"FileDetailsPanel"];

  if (self)
  {
    [self.window setTitle:@"File Details"];
  }

  return self;
}

- (void)windowDidLoad
{
  [super windowDidLoad];
}

- (void)awakeFromNib
{
  LOG_PROGRESS(@"Nib file is loaded");
  
  [self.window setTitle:@"File Details"];
}

#pragma mark - Custom Accessors
#pragma mark - IBActions
#pragma mark - Notification handlers
#pragma mark - Public
#pragma mark - Private
#pragma mark - Protocol conformance
#pragma mark - XyzDelegate (insert)
#pragma mark - NSCopying
#pragma mark - NSObject
#pragma mark - ...

@end
