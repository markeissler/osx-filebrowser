//
//  AppController.m
//  FileBrowser
//
//  Created by Mark Eissler on 8/6/15.
//  Copyright (c) 2015 Mark Eissler. All rights reserved.
//

#import "AppController.h"
#import "MainWindowDelegate.h"
#import "MainView.h"
#import "DirectorySelectViewController.h"
#import "DirectoryBrowseViewController.h"
#import "Konstants.h"
#import "Macros.h"

@interface AppController ()

@property (strong) MainWindowDelegate *mainWindowDelegate;
@property (weak) IBOutlet MainView *mainViewContainer;
@property (strong) NSMutableArray *mainViewControllers;

// add properties for menus (IBOutlets) here

// notification handlers
- (void)browseViewDidClose:(NSNotification *)notification;
- (void)directoryDidChange:(NSNotification *)notification;

// private methods
- (void)displayMainViewController:(NSViewController *)mainViewController;

@end

@implementation AppController

#pragma mark - Class Methods

#pragma mark - Lifecycle

- (instancetype)init
{
  self = [super initWithWindowNibName:@"MainWindow"];

  if (self)
  {
    _mainWindowDelegate = [[MainWindowDelegate alloc] init];
    _mainViewControllers = [[NSMutableArray alloc] init];
  }

  return self;
}

- (void)dealloc
{
  NSNotificationCenter *notificationCenter =
    [NSNotificationCenter defaultCenter];

  [notificationCenter removeObserver:self];
}

- (void)windowDidLoad
{
  [super windowDidLoad];

  self.window.restorable = NO;
  self.window.delegate = self.mainWindowDelegate;

  NSViewController *browseViewController =
    [[DirectoryBrowseViewController alloc] initWithWindow:self.window];
  [_mainViewControllers insertObject:browseViewController
     atIndex:MainViewDirectoryBrowseTag];

  NSViewController *selectViewController =
    [[DirectorySelectViewController alloc] initWithWindow:self.window];
  [_mainViewControllers insertObject:selectViewController
     atIndex:MainViewDirectorySelectTag];

  // register to receive directory update notifications
  NSNotificationCenter *notificationCenter =
    [NSNotificationCenter defaultCenter];
  [notificationCenter addObserver:self
     selector:@selector(directoryDidChange:)
     name:kMIX_DirectorySelectViewControllerDirectoryDidChange
     object:nil];
  
  // register to receive browse view close notifications
  [notificationCenter addObserver:self
     selector:@selector(browseViewDidClose:)
     name:kMIX_DirectoryBrowseViewControllerViewDidClose
     object:nil];

  // call display for each metaviewcontroller to force load (in reverse order!)
  [self displayMainViewController:browseViewController];
  [self displayMainViewController:selectViewController];
}

#pragma mark - Custom Accessors

#pragma mark - IBActions

- (IBAction)directoryBrowserViewRefresh:(id)sender
{
  DirectoryBrowseViewController *viewController =
    [self.mainViewControllers objectAtIndex:MainViewDirectoryBrowseTag];

  [viewController performSelector:@selector(reloadDirectory:)
     withObject:self
     afterDelay:0.0];
}

- (IBAction)directoryBrowserViewOpenSelectedFile:(id)sender
{
  DirectoryBrowseViewController *viewController =
    [self.mainViewControllers objectAtIndex:MainViewDirectoryBrowseTag];

  [viewController performSelector:@selector(openSelectedFile:)
     withObject:self
     afterDelay:0.0];
}

- (IBAction)directoryBrowserViewShowSelectedFileInFinder:(id)sender
{
  DirectoryBrowseViewController *viewController =
    [self.mainViewControllers objectAtIndex:MainViewDirectoryBrowseTag];

  [viewController performSelector:@selector(showSelectedFileInFinder:)
     withObject:self
     afterDelay:0.0];
}

- (IBAction)directoryBrowserViewCopySelectedFilePath:(id)sender
{
  DirectoryBrowseViewController *viewController =
    [self.mainViewControllers objectAtIndex:MainViewDirectoryBrowseTag];

  [viewController performSelector:@selector(copySelectedFilePath:)
     withObject:self
     afterDelay:0.0];
}

- (IBAction)directoryBrowserViewShowSelectedFileDetails:(id)sender
{
  DirectoryBrowseViewController *viewController =
    [self.mainViewControllers objectAtIndex:MainViewDirectoryBrowseTag];

  [viewController performSelector:@selector(showSelectedFileDetails:)
     withObject:self
     afterDelay:0.0];
}

#pragma mark - Notification handlers

- (void)browseViewDidClose:(NSNotification *)notification
{
  NSString *directoryPath =
    [[notification userInfo] objectForKey:@"directoryPath"];

  if (!directoryPath)
  {
    directoryPath = @"";
  }

  DirectorySelectViewController *viewController =
    [self.mainViewControllers objectAtIndex:MainViewDirectorySelectTag];
  [viewController configureForDirectory:directoryPath];
  [self displayMainViewController:viewController];
}

- (void)directoryDidChange:(NSNotification *)notification
{
  NSString *directoryPath =
    [[notification userInfo] objectForKey:@"directoryPath"];

  if (!directoryPath || directoryPath.length == 0)
  {
    return;
  }

  DirectoryBrowseViewController *viewController =
    [self.mainViewControllers objectAtIndex:MainViewDirectoryBrowseTag];
  [viewController loadFilesForDirectory:directoryPath];
  [self displayMainViewController:viewController];
}

#pragma mark - Public

#pragma mark - Private

- (void)displayMainViewController:(NSViewController *)mainViewController
{
  // try to end editing
  NSWindow *window = [self window];
  BOOL ended = [window makeFirstResponder:window];
  
  if (!ended) {
    return;
  }
  
#if DEBUG_LAYOUT
  // resize the mainWindow and mainViewContainer to accommodate content
  NSRect newWindowFrame = window.frame;
  
  newWindowFrame.origin = NSMakePoint(NSMinX(window.frame), NSMaxY(window.frame) - NSHeight(mainViewController.view.frame));
  newWindowFrame.size.height = mainViewController.view.frame.size.height + 20;
  newWindowFrame.size.width = mainViewController.view.frame.size.width;

  [window setFrame:newWindowFrame display:YES animate:YES];
#endif
  
  // put the view in the mainViewContainer
  NSView *view = [mainViewController view];
  [self.mainViewContainer setContentView:view];
  
  // enable/disable the window zoom button
  if ([mainViewController isKindOfClass:[DirectorySelectViewController class]]) {
    [self.window standardWindowButton:NSWindowZoomButton].enabled = NO;
    self.window.minSize = self.window.frame.size;
    self.window.maxSize = self.window.frame.size;
  } else {
    [self.window standardWindowButton:NSWindowZoomButton].enabled = YES;
    self.window.minSize = self.window.frame.size;
    self.window.maxSize = [[NSScreen mainScreen] frame].size;
  }
}

#pragma mark - Protocol conformance
#pragma mark - XyzDelegate (insert)
#pragma mark - NSCopying
#pragma mark - NSObject
#pragma mark - ...


@end
