//
//  DirectoryViewController.m
//  FileBrowser
//
//  Created by Mark Eissler on 8/6/15.
//  Copyright (c) 2015 Mark Eissler. All rights reserved.
//

#import "DirectoryViewController.h"
#import "Macros.h"

@interface DirectoryViewController () {
  BOOL _visible;
}

@property (readwrite, assign, getter=isVisible) BOOL visible;

@end

@implementation DirectoryViewController

#pragma mark - Class Methods

#pragma mark - Lifecycle

- (void)viewDidLoad
{
  [super viewDidLoad];
}

- (void)viewDidAppear
{
  self.visible = YES;
}

- (void)viewDidDisappear
{
  self.visible = NO;
}

#pragma mark - Custom Accessors

- (BOOL)isVisible
{
  return (_visible && self.view.window);
}

- (void)setVisible:(BOOL)visible
{
  @synchronized(self) {
    _visible = visible;
  }
}

#pragma mark - IBActions
#pragma mark - Notification handlers
#pragma mark - Public
#pragma mark - Private
#pragma mark - Protocol conformance
#pragma mark - XyzDelegate (insert)
#pragma mark - NSCopying
#pragma mark - NSObject
#pragma mark - ...

@end
