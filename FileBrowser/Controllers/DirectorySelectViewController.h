//
//  DirectorySelectViewController.h
//  FileBrowser
//
//  Created by Mark Eissler on 8/5/15.
//  Copyright (c) 2015 Mark Eissler. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DirectoryViewController.h"

#pragma mark - Konstants (extern)

/**
 *  @brief Posted when the DirectorySelectViewController object's selected
 *  directory has changed.
 *
 *  The notification object is the DirectorySelectViewController object whose
 *  directory selection has changed.This notification contains a userInfo
 *  dictionary with the following keys:
 *    - directoryPath (directory path selected)
 *    .
 */
extern NSString * const kMIX_DirectorySelectViewControllerDirectoryDidChange;

/**
 *  Directory select view controller.
 */
@interface DirectorySelectViewController : DirectoryViewController

#pragma mark - Class methods

#pragma mark - Lifecycle

/**
 *  @brief Initializes a new DirectorySelectViewController object for window.
 *
 *  @param window Reference to parent NSWindow.
 *
 *  @return Initialized DirectorySelectViewController object on success, nil on
 *  failure.
 *
 *  @throws "<NSInvalidArgumentException>" When window is nil.
 */
- (instancetype)initWithWindow:(NSWindow *)window;

#pragma mark - Custom Accessors
#pragma mark - IBActions

#pragma mark - Public

/**
 *  @brief Set the initial directory path.
 *
 *  @param path Directory path.
 */
- (void)configureForDirectory:(NSString *)path;

@end
