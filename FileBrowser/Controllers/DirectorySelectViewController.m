//
//  DirectorySelectViewController.m
//  FileBrowser
//
//  Created by Mark Eissler on 8/5/15.
//  Copyright (c) 2015 Mark Eissler. All rights reserved.
//

#import "DirectorySelectViewController.h"
#import "FileHelpers.h"
#import "Macros.h"

#pragma mark - Konstants

//
// notification messages
//
NSString * const kMIX_DirectorySelectViewControllerDirectoryDidChange =
  @"kMIX_DirectorySelectViewControllerDirectoryDidChange";

//
// kvo contexts
//
static void *kMIX_DirectorySelectViewControllerKVOCtxt_directory;

@interface DirectorySelectViewController ()

@property (weak) NSWindow *parentWindow;
@property (weak) IBOutlet NSTextField *directoryTextFieldLabel;
@property (weak) IBOutlet NSTextField *directoryTextField;
@property (weak) IBOutlet NSButton *directoryTextFieldButton;
@property (weak) IBOutlet NSTextField *errorMessageTextField;
@property (weak) IBOutlet NSButton *cancelButton;
@property (weak) IBOutlet NSButton *acceptButton;

// private methods
- (IBAction)selectDirectory:(id)sender;
- (IBAction)cancelOperation:(id)sender;
- (IBAction)acceptSelection:(id)sender;

- (void)displayErrorMessage:(NSString *)message;

// notifications
- (void)directoryTextFieldContentDidChange:(NSNotification *)notification;

@end

@implementation DirectorySelectViewController

#pragma mark - Class Methods

#pragma mark - Lifecycle

- (instancetype)init
{
  @throw [NSException exceptionWithName:@"WrongInitializer"
                                 reason:[NSString stringWithFormat:@"Call - [%@ initWithWindow]",
                                         [self class]]
                               userInfo:nil];
  
  return nil;
}

- (instancetype)initWithWindow:(NSWindow *)window
{
  if (!window) {
    @throw [NSException exceptionWithName:@"NSInvalidArgumentException"
              reason:@"Expected a window reference, got nil instead"
              userInfo:nil];
  }
  
  self = [super initWithNibName:@"DirectorySelectView" bundle:nil];

  if (self)
  {
    _parentWindow = window;
  }

  return self;
}

- (instancetype)init_p
{  
  return [super init];
}

- (void)dealloc
{
  NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
  [notificationCenter removeObserver:self];
  
  [self.directoryTextField removeObserver:self forKeyPath:@"stringValue"];
}

- (void)viewDidLoad
{
  [super viewDidLoad];

  NSNotificationCenter *notificationCenter =
    [NSNotificationCenter defaultCenter];
  [notificationCenter addObserver:self
     selector:@selector(directoryTextFieldContentDidChange:)
     name:NSControlTextDidChangeNotification
     object:self.directoryTextField];

  // observe changes on directoryTextField, this will catch updates set via
  // NSTextField::setStringValue: method which will be sent through the same
  // directoryTextFieldContentDidChange: notification handler later.
  [self.directoryTextField
     addObserver:self
     forKeyPath:@"stringValue"
     options:0
     context:&kMIX_DirectorySelectViewControllerKVOCtxt_directory];

  self.title = @"Directory Selection";
  self.directoryTextField.placeholderString = @"Directory";
  self.errorMessageTextField.hidden = YES;
  self.cancelButton.enabled = YES;
  self.acceptButton.keyEquivalent = @"\r";
  self.acceptButton.enabled = NO;
}

- (void)viewDidAppear
{
  [super viewDidAppear];
  
  [self.parentWindow makeFirstResponder:self.directoryTextField];
}

#pragma mark - Custom Accessors

#pragma mark - IBActions

- (IBAction)selectDirectory:(id)sender
{
  NSOpenPanel *openPanel = [NSOpenPanel openPanel];

  openPanel.canChooseFiles = NO;
  openPanel.canChooseDirectories = YES;

  [openPanel beginSheetModalForWindow:self.parentWindow
     completionHandler:^(NSInteger result) {
       if (result == NSModalResponseOK)
       {
         NSURL *url = openPanel.URL;
         self.directoryTextField.stringValue = url.path;
       }
     }];
}

- (IBAction)cancelOperation:(id)sender
{
  [self.parentWindow close];
}

- (IBAction)acceptSelection:(id)sender
{
  NSString *directoryPath = self.directoryTextField.stringValue;
  BOOL directoryExists = [FileHelpers directoryExists:directoryPath];

  if (directoryExists)
  {
    // send notification to appcontroller
    NSNotificationCenter *notificationCenter =
      [NSNotificationCenter defaultCenter];
    NSDictionary *userInfo = @{
      @"directoryPath": [FileHelpers resolvePath:directoryPath]
    };
    [notificationCenter postNotificationName:kMIX_DirectorySelectViewControllerDirectoryDidChange
       object:self
       userInfo:userInfo];
  } else {
    NSBeep();
    [self displayErrorMessage:@"Invalid directory!"];
  }
}

#pragma mark - Notification handlers

- (void)directoryTextFieldContentDidChange:(NSNotification *)notification
{
  // clear error message
  [self displayErrorMessage:nil];
  
  if (self.directoryTextField.stringValue.length > 0) {
    self.acceptButton.enabled = YES;
  } else {
    self.acceptButton.enabled = NO;
  }
}

#pragma mark - Public

- (void)configureForDirectory:(NSString *)path
{
  NSAssert([FileHelpers directoryExists:path], @"path must point to a directory");
  
  self.directoryTextField.stringValue = path;
}

#pragma mark - Private

- (void)displayErrorMessage:(NSString *)message
{
  if (message && message.length > 0) {
    self.errorMessageTextField.stringValue = message;
    self.errorMessageTextField.hidden = NO;
  } else {
    self.errorMessageTextField.stringValue = @"";
    self.errorMessageTextField.hidden = YES;
  }
}

#pragma mark - Protocol conformance (NSKeyValueObserving)

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
  change:(NSDictionary *)change context:(void *)context
{
  if (context != &kMIX_DirectorySelectViewControllerKVOCtxt_directory)
  {
    [super observeValueForKeyPath:keyPath
       ofObject:object
       change:change
       context:context];

    return;
  }

  if (context == &kMIX_DirectorySelectViewControllerKVOCtxt_directory)
  {
    [self directoryTextFieldContentDidChange:nil];
  }
}

#pragma mark - XyzDelegate (insert)
#pragma mark - NSCopying
#pragma mark - NSObject
#pragma mark - ...

@end
