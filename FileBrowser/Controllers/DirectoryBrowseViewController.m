//
//  DirectoryBrowseViewController.m
//  FileBrowser
//
//  Created by Mark Eissler on 8/5/15.
//  Copyright (c) 2015 Mark Eissler. All rights reserved.
//

#import "DirectoryBrowseViewController.h"
#import "FileDetailsPanelController.h"
#import "MIXTableView.h"
#import "MIXFile.h"
#import "FileHelpers.h"
#import "Macros.h"

#define TRIGGER_SEARCH_VIA_NOTIFY

#pragma mark - Konstants

//
// notification messages
//
NSString * const kMIX_DirectoryBrowseViewControllerViewDidClose =
  @"kMIX_DirectoryBrowseViewControllerViewDidClose";
NSString * const kMIX_DirectoryBrowseViewControllerViewDidOpen =
  @"kMIX_DirectoryBrowseViewControllerViewDidOpen";
NSString * const kMIX_DirectoryBrowseViewControllerViewDidEmpty =
  @"kMIX_DirectoryBrowseViewControllerViewDidEmpty";
NSString * const kMIX_DirectoryBrowseViewControllerViewDidPopulate =
  @"kMIX_DirectoryBrowseViewControllerViewDidPopulate";
NSString * const kMIX_DirectoryBrowseViewControllerSelectionDidChange =
  @"kMIX_DirectoryBrowseViewControllerSelectionDidChange";

//
// kvo contexts
//
static void *kMIX_DirectorySelectViewControllerKVOCtxt_directoryFilesIndex;
static void *kMIX_DirectorySelectViewControllerKVOCtxt_directoryFilesIndexes;
static void *kMIX_DirectorySelectViewControllerKVOCtxt_directoryFilesCount;
static void *kMIX_DirectorySelectViewControllerKVOCtxt_operationQueueCount;
static void *kMIX_DirectorySelectViewControllerKVOCtxt_recurseToggleState;

//
// NSOperationQueue operation names
//
NSString * const kMIX_DirectoryBrowseViewControllerOPQName_filesLoader =
  @"kMIX_DirectoryBrowseViewControllerOpQueueFilesLoader";

@interface DirectoryBrowseViewController ()

@property (weak) NSWindow *parentWindow;
@property (copy) NSString *directoryPath;
@property (strong) NSPredicate *searchFilterPredicate;
@property (strong) NSMenu *contextMenu;
@property (strong) FileDetailsPanelController *fileDetailsPanelController;

@property (weak) IBOutlet NSButton *subdirectoryRecurseCheckbox;
@property (weak) IBOutlet NSSearchField *filesSearchField;
@property (weak) IBOutlet MIXTableView *filesTableView;
@property (weak) IBOutlet NSProgressIndicator *progressIndicator;

@property (strong) IBOutlet NSArrayController *filesArrayController;

@property (weak) IBOutlet NSButton *cancelButton;
@property (weak) IBOutlet NSButton *acceptButton;

// private methods
- (NSMutableArray *)files;
- (void)setFiles:(NSArray *)files;
- (void)addFile:(MIXFile *)file;
- (NSOperationQueue *)operationQueue;
- (void)setOperationQueue:(NSOperationQueue *)queue;

- (void)openFile:(MIXFile *)file;
- (void)showFileInFinder:(MIXFile *)file;
- (void)copyFilePath:(MIXFile *)file;
- (void)showFileDetails:(MIXFile *)file;

// notifications
- (void)fileSearchFieldDidBeginEditing:(NSNotification *)notification;

@end

@implementation DirectoryBrowseViewController

#pragma mark - Class Methods

#pragma mark - Lifecycle

- (instancetype)init
{
  @throw [NSException exceptionWithName:@"WrongInitializer"
                                 reason:[NSString stringWithFormat:@"Call - [%@ initWithWindow]",
                                         [self class]]
                               userInfo:nil];
  
  return nil;
}

- (instancetype)initWithWindow:(NSWindow *)window
{
  if (!window)
  {
    @throw [NSException exceptionWithName:@"NSInvalidArgumentException"
              reason:@"Expected a window reference, got nil instead"
              userInfo:nil];
  }

  self = [super initWithNibName:@"DirectoryBrowseView" bundle:nil];

  if (self)
  {
    _parentWindow = window;
    _directoryPath = @"";
    _files = [[NSMutableArray alloc] init];
    _operationQueue = [[NSOperationQueue alloc] init];
    _operationQueue.maxConcurrentOperationCount = 2;
    _contextMenu = [[NSMenu alloc] initWithTitle:@"File Action"];
    
    // lazy load fileDetailsPanelController
    _fileDetailsPanelController = nil;

    // search predicate is case- and diacritic-insensitive
    _searchFilterPredicate =
      [NSPredicate predicateWithFormat:@"(name contains[cd] $searchString)"];
  }

  return self;
}

- (instancetype)init_p
{  
  return [super init];
}

- (void)dealloc
{
  NSNotificationCenter *notificationCenter =
    [NSNotificationCenter defaultCenter];
  [notificationCenter removeObserver:self];

  [self.filesArrayController removeObserver:self forKeyPath:@"selectionIndex"];

  [self.filesArrayController removeObserver:self forKeyPath:@"selectionIndexes"];

  [self.filesArrayController
     removeObserver:self
     forKeyPath:@"arrangedObjects.count"];

  [self.operationQueue removeObserver:self forKeyPath:@"operationCount"];

  [self.subdirectoryRecurseCheckbox
     removeObserver:self
     forKeyPath:@"cell.state"];
}

- (void)viewDidLoad
{
  [super viewDidLoad];

  NSNotificationCenter *notificationCenter =
    [NSNotificationCenter defaultCenter];
  [notificationCenter addObserver:self
     selector:@selector(fileSearchFieldDidBeginEditing:)
     name:NSControlTextDidBeginEditingNotification
     object:self.filesSearchField];
  [notificationCenter addObserver:self
     selector:@selector(fileSearchFieldDidEndEditing:)
     name:NSControlTextDidEndEditingNotification
     object:self.filesSearchField];

  // observe selectionIndex(es) and count in filesArrayController to update UI
  [self.filesArrayController
     addObserver:self
     forKeyPath:@"selectionIndex"
     options:0
     context:&kMIX_DirectorySelectViewControllerKVOCtxt_directoryFilesIndex];
  
  [self.filesArrayController
     addObserver:self
     forKeyPath:@"selectionIndexes"
     options:0
     context:&kMIX_DirectorySelectViewControllerKVOCtxt_directoryFilesIndexes];

  [self.filesArrayController
     addObserver:self
     forKeyPath:@"arrangedObjects.count"
     options:0
     context:&kMIX_DirectorySelectViewControllerKVOCtxt_directoryFilesCount];

  // monitor the operation queue to control the spinner
  [self.operationQueue
     addObserver:self
     forKeyPath:@"operationCount"
     options:0
     context:&kMIX_DirectorySelectViewControllerKVOCtxt_operationQueueCount];

  // observe the recurse checkbox to update the UI
  [self.subdirectoryRecurseCheckbox
     addObserver:self
     forKeyPath:@"cell.state"
     options:0
     context:&kMIX_DirectorySelectViewControllerKVOCtxt_recurseToggleState];

  // sort array by name initially
  self.filesTableView.sortDescriptors = @[
    [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES]
  ];

  self.title = @"Directory Browser";
  self.subdirectoryRecurseCheckbox.state = NSOffState;
  self.subdirectoryRecurseCheckbox.action =
    @selector(toggleSubdirectoryRecurse:);
  self.filesSearchField.enabled = NO;
#ifndef TRIGGER_SEARCH_VIA_NOTIFY
  self.filesSearchField.action = @selector(searchFiles:);
#endif
  self.filesTableView.delegate = self;
  self.filesTableView.enabled = NO;
  self.cancelButton.enabled = YES;
  self.acceptButton.keyEquivalent = @"\r";
  self.acceptButton.enabled = NO;
  self.progressIndicator.hidden = YES;

  // context menu
  [self.contextMenu
     addItem:[[NSMenuItem alloc] initWithTitle:@"Open"
                action:@selector(openClickedFile:)
                keyEquivalent:@""]];
  
  [self.contextMenu
     addItem:[[NSMenuItem alloc] initWithTitle:@"Show in Finder"
                action:@selector(showClickedFileInFinder:)
                keyEquivalent:@""]];
  
  [self.contextMenu addItem:[NSMenuItem separatorItem]];

  [self.contextMenu
     addItem:[[NSMenuItem alloc] initWithTitle:@"Copy Path"
                action:@selector(copyClickedFilePath:)
                keyEquivalent:@""]];

  [self.contextMenu
     addItem:[[NSMenuItem alloc] initWithTitle:@"Details"
                action:@selector(showClickedFileDetails:)
                keyEquivalent:@""]];
  
  [self.filesTableView setMenu:self.contextMenu];
}

- (void)viewDidAppear
{
  [super viewDidAppear];
  
  NSNotificationCenter *notificationCenter =
    [NSNotificationCenter defaultCenter];

  // trigger update for selection so observers can perform UI updates
  NSDictionary *userInfo = @{
    @"itemCount": [NSNumber numberWithUnsignedInteger:self.filesArrayController.selectionIndexes.count]
  };
  [notificationCenter postNotificationName:kMIX_DirectoryBrowseViewControllerSelectionDidChange
     object:self
     userInfo:userInfo];
  
  userInfo = @{
    @"directoryPath": self.directoryPath
  };
  [notificationCenter postNotificationName:kMIX_DirectoryBrowseViewControllerViewDidOpen
     object:self
     userInfo:userInfo];
}

- (void)viewDidDisappear
{
  // trigger update for selection so observers can perform UI updates
  NSNotificationCenter *notificationCenter =
    [NSNotificationCenter defaultCenter];
  NSDictionary *userInfo = @{
    @"itemCount": [NSNumber numberWithUnsignedInteger:0]
  };
  [notificationCenter postNotificationName:kMIX_DirectoryBrowseViewControllerSelectionDidChange
     object:self
     userInfo:userInfo];
}

#pragma mark - Custom Accessors

- (NSMutableArray *)files
{
  return _files;
}

- (void)setFiles:(NSArray *)files
{
  if (files == _files)
  {
    return;
  }

  [_files removeAllObjects];
  [_files addObjectsFromArray:files];

  // tell the controller to update its view
  [_filesArrayController rearrangeObjects];
}

- (void)addFile:(MIXFile *)file
{
  [self.files addObject:file];
  [self.filesArrayController rearrangeObjects];
}

- (NSOperationQueue *)operationQueue
{
  return _operationQueue;
}

- (void)setOperationQueue:(NSOperationQueue *)queue
{
  if (queue == _operationQueue)
  {
    return;
  }

  _operationQueue = queue;
}

#pragma mark - IBActions

- (IBAction)toggleSubdirectoryRecurse:(id)sender
{
  // halt file load operation
  for (NSOperation *operation in self.operationQueue.operations)
  {
    if ([operation.name
           isEqualToString:kMIX_DirectoryBrowseViewControllerOPQName_filesLoader])
    {
      [operation cancel];
    }
  }

  [self reloadDirectory:nil];
}

- (IBAction)reloadDirectory:(id)sender
{
  NSOperation *operation = nil;

  for (operation in self.operationQueue.operations)
  {
    if ([operation.name
           isEqualToString:kMIX_DirectoryBrowseViewControllerOPQName_filesLoader])
    {
      break;
    } else {
      operation = nil;
    }
  }

  if (operation && !operation.isCancelled)
  {
    [self performSelector:@selector(reloadDirectory:)
       withObject:self
       afterDelay:1.0];
  } else {
    [self loadFilesForDirectory:self.directoryPath refresh:YES];
  }
}

- (IBAction)searchFiles:(id)sender
{
  NSString *searchString = self.filesSearchField.stringValue;
  NSPredicate *predicate = nil;

  if (searchString.length > 0)
  {
    NSMutableDictionary *searchDict = [NSMutableDictionary dictionary];
    [searchDict setObject:searchString forKey:@"searchString"];

    // create a predicate from template, replacing all variables
    predicate =
      [self.searchFilterPredicate predicateWithSubstitutionVariables:searchDict];
  }

  self.filesArrayController.filterPredicate = predicate;

#ifdef TRIGGER_SEARCH_VIA_NOTIFY
  // maintain key window status after every search
  if (self.filesSearchField != [self.parentWindow firstResponder])
  {
    [self.parentWindow makeFirstResponder:self.filesSearchField];
  }
#endif
}

- (IBAction)openSelectedFiles:(id)sender
{
  NSIndexSet *indexes = self.filesArrayController.selectionIndexes;

  [indexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
       MIXFile *file = [self.filesArrayController.arrangedObjects
                          objectAtIndex:idx];

       if (!file || !file.resolvedPath)
       {
         return;
       }

       [[NSWorkspace sharedWorkspace] openFile:file.path];
     }];
}

- (IBAction)cancelOperation:(id)sender
{
  // halt the operation queue
  [self.operationQueue cancelAllOperations];
  
  NSNotificationCenter *notificationCenter =
    [NSNotificationCenter defaultCenter];
  NSDictionary *userInfo = @{
    @"directoryPath": self.directoryPath
  };
  [notificationCenter postNotificationName:kMIX_DirectoryBrowseViewControllerViewDidClose
     object:self
     userInfo:userInfo];
}

- (IBAction)acceptSelection:(id)sender
{
  [self performSelector:@selector(openSelectedFiles:) withObject:self afterDelay:0.0];
}

- (IBAction)openClickedFile:(id)sender
{
  [self openFile:[self clickedFile]];
}

- (IBAction)showClickedFileInFinder:(id)sender
{
  [self showFileInFinder:[self clickedFile]];
}

- (IBAction)copyClickedFilePath:(id)sender
{
  [self copyFilePath:[self clickedFile]];
}

- (IBAction)showClickedFileDetails:(id)sender
{
  [self showFileDetails:[self clickedFile]];
}

- (IBAction)openSelectedFile:(id)sender
{
  [self openFile:[self selectedFile]];
}

- (IBAction)showSelectedFileInFinder:(id)sender
{
  [self showFileInFinder:[self selectedFile]];
}

- (IBAction)copySelectedFilePath:(id)sender
{
  [self copyFilePath:[self selectedFile]];
}

- (IBAction)showSelectedFileDetails:(id)sender
{
  [self showFileDetails:[self selectedFile]];
}

#pragma mark - Notification handlers

- (void)fileSearchFieldDidBeginEditing:(NSNotification *)notification
{
  // clear the selection in filesTableView
  self.filesArrayController.selectionIndex = NSNotFound;
}

- (void)fileSearchFieldDidEndEditing:(NSNotification *)notification
{
#ifdef TRIGGER_SEARCH_VIA_NOTIFY
  // trigger selector *after* editing has completed
  [self performSelector:@selector(searchFiles:) withObject:nil];
#endif
}

#pragma mark - Public

- (MIXFile *)clickedFile
{
  NSInteger row = [self.filesTableView clickedRow];

  if (row < 0)
  {
    return nil;
  }

  MIXFile *file = [[self.filesArrayController
                      arrangedObjects] objectAtIndex:row];

  return file;
}

- (MIXFile *)selectedFile
{
  NSUInteger index = self.filesArrayController.selectionIndex;
  
  if (index == NSNotFound) {
    return nil;
  }
  
  MIXFile *file = [[self.filesArrayController arrangedObjects] objectAtIndex:index];
  
  return file;
}

- (void)loadFilesForDirectory:(NSString *)path
{
  return [self loadFilesForDirectory:path refresh:NO];
}

- (void)loadFilesForDirectory:(NSString *)path refresh:(BOOL)refresh
{
  NSAssert(path.length > 0, @"path must not be nil or empty");
  NSAssert([FileHelpers directoryExists:path],
    @"path must point to a directory");

  NSInteger previousFileCount = self.files.count;
  __weak __typeof__(self) weakSelf = self;
  
  if (!refresh && [path isEqualToString:self.directoryPath]
    && self.files.count > 0)
  {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
         __strong __typeof__(self) strongSelf = weakSelf;
         [strongSelf.parentWindow
              makeFirstResponder:strongSelf.filesTableView];
       }];

    return;
  }

  [[NSOperationQueue mainQueue] addOperationWithBlock:^{
       __strong __typeof__(self) strongSelf = weakSelf;
       if (strongSelf)
       {
         strongSelf.files = @[];
       }
     }];

  self.directoryPath = path;
  NSURL *directoryURL = [NSURL fileURLWithPath:self.directoryPath];

  // @FIXME: define error handler for file load
  BOOL (^ errorHandler)(NSURL *, NSError *) = ^BOOL (NSURL *url,
      NSError *error) {
    LOG_PROGRESS(@"something bad happened while enumerating dir!");

    return NO;
  };

  // configure retrieval depth
  NSUInteger enumeratorOptions = NSDirectoryEnumerationSkipsHiddenFiles;
  if (self.subdirectoryRecurseCheckbox.state == NSOffState)
  {
    enumeratorOptions = enumeratorOptions |
      NSDirectoryEnumerationSkipsSubdirectoryDescendants;
  }

  // load files asynchronously
  NSBlockOperation *operation = [[NSBlockOperation alloc] init];
  operation.name = kMIX_DirectoryBrowseViewControllerOPQName_filesLoader;
  __weak NSBlockOperation *weakOperation = operation;
  [operation addExecutionBlock:^{
       __strong __typeof__(self) strongSelf = weakSelf;
       if (strongSelf)
       {
         NSFileManager *fileManager = [[NSFileManager alloc] init];
         NSDirectoryEnumerator *directoryEnumerator = [fileManager enumeratorAtURL:directoryURL
                                                         includingPropertiesForKeys:nil
                                                         options:enumeratorOptions
                                                         errorHandler:errorHandler];
         NSNumber *isDirectory = nil;
         for (NSURL *url in directoryEnumerator)
         {
           if (!weakOperation || weakOperation.isCancelled)
           {
             return;
           }

           // skip directory entries
           [url getResourceValue:&isDirectory
              forKey:NSURLIsDirectoryKey
              error:nil];
           if ([isDirectory boolValue])
           {
             continue;
           }

           LOG_PROGRESS(@"processing file: ", [NSString stringWithFormat:@"%@",
                                                 [url lastPathComponent]]);

           // view (updating file list) must be updated in main thread
           MIXFile *file = [MIXFile fileWithURL:url];
           [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [strongSelf addFile:file];
              }];
         }

         // view (enabling disabling searchField) must be updated in main thread
         [[NSOperationQueue mainQueue] addOperationWithBlock:^{
              NSNotificationCenter *notificationCenter =
                [NSNotificationCenter defaultCenter];
              NSDictionary *userInfo = @{
                @"itemCount": [NSNumber numberWithUnsignedInteger:previousFileCount]
              };
           
              if (strongSelf.files.count > 0)
              {
                if (previousFileCount == 0)
                {
                  [notificationCenter postNotificationName:kMIX_DirectoryBrowseViewControllerViewDidPopulate
                     object:strongSelf
                     userInfo:userInfo];
                }
                strongSelf.filesSearchField.enabled = YES;
              } else {
                if (previousFileCount > 0)
                {
                  [notificationCenter postNotificationName:kMIX_DirectoryBrowseViewControllerViewDidEmpty
                     object:strongSelf
                     userInfo:userInfo];
                }
                strongSelf.filesSearchField.enabled = NO;
              }
            }];
       } // (strongSelf)
     }];

  [self.operationQueue addOperation:operation];
}

#pragma mark - Private

- (void)openFile:(MIXFile *)file
{
  if (!file || !file.resolvedPath) {
    return;
  }

  [[NSWorkspace sharedWorkspace] openFile:file.path];
}

- (void)showFileInFinder:(MIXFile *)file
{
  if (!file || !file.url) {
    return;
  }
  
  [[NSWorkspace sharedWorkspace] activateFileViewerSelectingURLs:@[file.url]];
}

- (void)copyFilePath:(MIXFile *)file
{
  if (!file || !file.resolvedPath) {
    return;
  }
  
  NSPasteboard *pasteboard = [NSPasteboard generalPasteboard];
  [pasteboard clearContents];
  [pasteboard writeObjects:@[file.resolvedPath]];
}

- (void)showFileDetails:(MIXFile *)file
{
  if (!self.fileDetailsPanelController) {
    self.fileDetailsPanelController = [[FileDetailsPanelController alloc] init];
  }
  
  self.fileDetailsPanelController.file = file;
  
  [self.fileDetailsPanelController showWindow:self];
}

#pragma mark - Protocol conformance (NSKeyValueObserving)

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
  change:(NSDictionary *)change context:(void *)context
{
  if (context == &kMIX_DirectorySelectViewControllerKVOCtxt_operationQueueCount) {
    if (self.operationQueue.operationCount > 0) {
      // animate in main thread
      dispatch_async(dispatch_get_main_queue(), ^{
        [self.progressIndicator startAnimation:self];
        self.progressIndicator.hidden = NO;
      });
    } else {
      // animate in main thread
      dispatch_async(dispatch_get_main_queue(), ^{
        [self.progressIndicator stopAnimation:self];
        self.progressIndicator.hidden = YES;
      });
    }
    
    return;
  }

  if (context == &kMIX_DirectorySelectViewControllerKVOCtxt_directoryFilesIndex)
  {
    NSUInteger selectedFileIndex = [self.filesArrayController selectionIndex];

    if (selectedFileIndex == NSNotFound)
    {
      self.acceptButton.enabled = NO;

      return;
    }

    // update UI
    self.acceptButton.enabled = YES;

    return;
  }
  
  if (context == &kMIX_DirectorySelectViewControllerKVOCtxt_directoryFilesIndexes)
  {
    NSNotificationCenter *notificationCenter =
      [NSNotificationCenter defaultCenter];
    NSDictionary *userInfo = @{
      @"itemCount": [NSNumber numberWithUnsignedInteger:self.filesArrayController.selectionIndexes.count]
    };
    [notificationCenter postNotificationName:kMIX_DirectoryBrowseViewControllerSelectionDidChange
       object:self
       userInfo:userInfo];

    return;
  }
  
  if (context == &kMIX_DirectorySelectViewControllerKVOCtxt_directoryFilesCount)
  {
    NSUInteger filecount = [self.filesArrayController.arrangedObjects count];
    // update UI
    if (filecount > 0) {
      self.filesTableView.enabled = YES;
      if (self.filesTableView != [self.parentWindow firstResponder])
      {
        [self.parentWindow makeFirstResponder:self.filesTableView];
      }
    } else {
      self.filesTableView.enabled = NO;
    }
    
    return;
  }
  
  if (context == &kMIX_DirectorySelectViewControllerKVOCtxt_recurseToggleState)
  {
    return;
  }
  
  [super observeValueForKeyPath:keyPath
     ofObject:object
     change:change
     context:context];
}

#pragma mark - MIXTableViewDelegate

- (BOOL)tableView:(NSTableView *)tableView shouldOpenContextMenu:(NSMenu *)menu
  alternateMenu:(NSMenu *__autoreleasing *)altMenu
{
  if (self.filesArrayController.selectionIndexes.count > 1)
  {
    return NO;
  }

  return YES;
}

#pragma mark - XyzDelegate (insert)
#pragma mark - NSCopying
#pragma mark - NSObject
#pragma mark - ...

@end
