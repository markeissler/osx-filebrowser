//
//  DirectoryViewController.h
//  FileBrowser
//
//  Created by Mark Eissler on 8/6/15.
//  Copyright (c) 2015 Mark Eissler. All rights reserved.
//

#import <Cocoa/Cocoa.h>

/**
 *  Base class for directory view controllers.
 */
@interface DirectoryViewController : NSViewController

/**
 *  @brief YES if the view has been loaded and has a parent window, 
 *  otherwise NO.
 */
@property (readonly, assign, getter=isVisible) BOOL visible;

@end
