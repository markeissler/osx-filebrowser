//
//  FileDetailsPanelController.h
//  FileBrowser
//
//  Created by Mark Eissler on 8/13/15.
//  Copyright (c) 2015 Mark Eissler. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class MIXFile;

@interface FileDetailsPanelController : NSWindowController

@property (readwrite, strong) MIXFile *file;

#pragma mark - Class methods
#pragma mark - Lifecycle
#pragma mark - Custom Accessors
#pragma mark - IBActions
#pragma mark - Public

@end
