//
//  AppController.h
//  FileBrowser
//
//  Created by Mark Eissler on 8/6/15.
//  Copyright (c) 2015 Mark Eissler. All rights reserved.
//

#import <Cocoa/Cocoa.h>

typedef NS_ENUM(NSUInteger, eMIX_FileBrowser_MainViewTag) {
  MainViewDirectoryBrowseTag = 0,
  MainViewDirectorySelectTag = 1
};

@interface AppController : NSWindowController

#pragma mark - Class methods
#pragma mark - Lifecycle
#pragma mark - Custom Accessors

#pragma mark - IBActions

- (IBAction)directoryBrowserViewRefresh:(id)sender;
- (IBAction)directoryBrowserViewOpenSelectedFile:(id)sender;
- (IBAction)directoryBrowserViewShowSelectedFileInFinder:(id)sender;
- (IBAction)directoryBrowserViewCopySelectedFilePath:(id)sender;
- (IBAction)directoryBrowserViewShowSelectedFileDetails:(id)sender;

#pragma mark - Public

@end
