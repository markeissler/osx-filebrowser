//
//  DirectoryBrowseViewController.h
//  FileBrowser
//
//  Created by Mark Eissler on 8/5/15.
//  Copyright (c) 2015 Mark Eissler. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DirectoryViewController.h"
#import "MIXTableViewDelegate.h"

@class MIXFile;

#pragma mark - Konstants (extern)

/**
 *  @brief Posted when the DirectoryBrowseViewController object's view is
 *  closed.
 *
 *  The notification object is the DirectoryBrowseViewController object whose
 *  view closed. This notification contains a userInfo dictionary with the
 *  following keys:
 *    - directoryPath (original directory path loaded)
 *    .
 */
extern NSString * const kMIX_DirectoryBrowseViewControllerViewDidClose;

/**
 *  @brief Posted when the DirectoryBrowseViewController object's view is
 *  opened.
 *
 *  The notification object is the DirectoryBrowseViewController object whose
 *  view closed. This notification contains a userInfo dictionary with the
 *  following keys:
 *    - directoryPath (directory path loaded)
 *    .
 */
extern NSString * const kMIX_DirectoryBrowseViewControllerViewDidOpen;

/**
 *  @brief Posted when the DirectoryBrowseViewController object's view is
 *  emptied after having been populated.
 *
 *  The notification object is the DirectoryBrowseViewController object whose
 *  view closed. This notification contains a userInfo dictionary with the
 *  following keys:
 *    - itemCount (number of items before empty)
 *    .
 */
extern NSString * const kMIX_DirectoryBrowseViewControllerViewDidEmpty;

/**
 *  @brief Posted when the DirectoryBrowseViewController object's view is
 *  populated after having been empty.
 *
 *  The notification object is the DirectoryBrowseViewController object whose
 *  view closed. This notification contains a userInfo dictionary with the
 *  following keys:
 *    - itemCount (number of items after populate)
 *    .
 */
extern NSString * const kMIX_DirectoryBrowseViewControllerViewDidPopulate;

/**
 *  @brief Posted when the DirectoryBrowseViewController object's selections
 *  have changed.
 *
 *  The notification object is the DirectoryBrowseViewController object whose
 *  selections have changed. This notification contains a userInfo dictionary
 *  with the following keys:
 *    - itemCount (number of items selected)
 *    .
 */
extern NSString * const kMIX_DirectoryBrowseViewControllerSelectionDidChange;

/**
 *  Directory browse view controller.
 */
@interface DirectoryBrowseViewController : DirectoryViewController <MIXTableViewDelegate> {
  @protected
  __strong NSMutableArray *_files;
  __strong NSOperationQueue *_operationQueue;
}

#pragma mark - Class methods

#pragma mark - Lifecycle

/**
 *  @brief Initializes a new DirectoryBrowseViewController object for window.
 *
 *  @param window Reference to parent NSWindow.
 *
 *  @return Initialized DirectoryBrowseViewController object on success, nil on
 *  failure.
 *
 *  @throws "<NSInvalidArgumentException>" When window is nil.
 */
- (instancetype)initWithWindow:(NSWindow *)window;

#pragma mark - Custom Accessors

#pragma mark - IBActions

// browser view actions
- (IBAction)toggleSubdirectoryRecurse:(id)sender;
- (IBAction)reloadDirectory:(id)sender;
- (IBAction)searchFiles:(id)sender;
- (IBAction)openSelectedFiles:(id)sender;
- (IBAction)cancelOperation:(id)sender;
- (IBAction)acceptSelection:(id)sender;

// context menu actions
- (IBAction)openClickedFile:(id)sender;
- (IBAction)showClickedFileInFinder:(id)sender;
- (IBAction)copyClickedFilePath:(id)sender;
- (IBAction)showClickedFileDetails:(id)sender;

// main menu actions
- (IBAction)openSelectedFile:(id)sender;
- (IBAction)showSelectedFileInFinder:(id)sender;
- (IBAction)copySelectedFilePath:(id)sender;
- (IBAction)showSelectedFileDetails:(id)sender;

#pragma mark - Public

/**
 *  @brief Returns the MIXFile object associated with the row that has been
 *  right-clicked.
 *
 *  @return MIXFile object on success, nil on failure.
 */
- (MIXFile *)clickedFile;

/**
 *  @brief Returns the MIXFile object associated with the row that has been
 *  selected in the table view.
 *
 *  @return MIXFile object on success, nil on failure.
 */
- (MIXFile *)selectedFile;

/**
 *  @brief Trigger a load of the browser view for files at path.
 *
 *  If the path has already been loaded, the results will be cached. To force
 *  a reload call #loadFilesForDirectory:refresh: with refresh set to YES.
 *
 *  @param path Directory path to load.
 *
 *  @note This is a convenience method that calls 
 *  #loadFilesForDirectory:refresh:.
 *
 *  @see #loadFilesForDirectory:refresh:
 */
- (void)loadFilesForDirectory:(NSString *)path;

/**
 *  @brief Trigger a load/reload of the browser view for files at path.
 *
 *  If the path has already been loaded and refresh is NO, cached data will be
 *  used instead of reloading the files at path. If the path has changed and
 *  refresh is NO, cached data will be purged and a reload will occur. If
 *  refresh is YES, cached data will be purged regardless of path and files will
 *  be reloaded.
 *
 *  @param path    Directory path to load.
 *  @param refresh YES to force reload of path, NO to use cached data.
 */
- (void)loadFilesForDirectory:(NSString *)path refresh:(BOOL)refresh;

@end
