//
//  main.m
//  FileBrowser
//
//  Created by Mark Eissler on 8/5/15.
//  Copyright (c) 2015 Mark Eissler. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
  return NSApplicationMain(argc, argv);
}
