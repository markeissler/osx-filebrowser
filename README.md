# FileBrowser OSX App

An OSX (Objective-C) prototype that implements a 3-stage wizard-style interface, which allows the user to type or pick a local folder and have that folder scanned for files. The list of files will be displayed in a table, which the the user can then filter and refresh. The user can also elect to perform some context-menu actions on selected files including: "Show in Finder", "Copy Path", "Details." Finally, the user can elect to open each file in the OS default viewer.

## Screens

1 - Directory selection

![](./imgs/1_directory_selection_s.png)

2 - Files filter and selection

![](./imgs/2_file_filter_and_selection_s.png)

3 - Context menu

![](./imgs/3_context_menu_s.png)

4 - File details

![](./imgs/4_context_details_s.png)

## Bindings
Cocoa bindings have been used where it makes the most sense to use them. That is, the files list view (an `NSTableView` backed by an `NSArrayController`) has been implemented this way but the search field has not. Initially, the search field had been implemented strictly with bindings but the user experience is not optimal: for example, the behavior of the search field is to lose focus whenever a match has been made, but the user may very well desire to continue refining the search term and requiring the user to reselect the search field is not ideal. This awkward behavior is cumbersome to customize when bindings are used without subclassing and modifying `NSSearchField`...which has not been done here.

The files details view has also been implemented with bindings.

## Search Field
Search field behavior has been implemented via notifications. The `DirectoryBrowseViewController` registers to receive notifications whenever the search field triggers a "DidEndEditing" event, this allows the app to trigger an update to the array controller's filter predicate at that time, followed by a request to once again make the search field the first responder (view focus) for the parent window.

By commenting (or `undefining`) the `TRIGGER_SEARCH_VIA_NOTIFY` constant at the top of the _DirectoryBrowseViewController.m_ implementation file search field behavior will revert to the standard experience albeit implemented via the traditional "action" design.

Finally, as implemented, the search field requires that the user press the `Enter` key to initiate a filter update. The entire search field text will be selected immediately following execution so that the user can type new search criteria or alter the existing string.

### Main Application Menu
The following main application menus have been configured as follows:

- File (hides inapplicable items)
- Edit (hides inapplicable items)
- View (adds a `Refresh`) command to refresh the view
- Browse (duplicates context menu)

### Context Menu
The context menu will only display when a single item or no item is selected in the browse directory view. An obvious enhancement would be to present a modified context menu for dealing with multiple selections.

### Browse Menu
The context menu is duplicated in the main application `Browse` menu. This menu will be enabled and disabled according to the same rules as the context menu.

### View Menu
The `View` menu contains a single command: `Refresh`. This item will be enabled only when the browse view is visible. The command forces a refresh of content.

### File Menu
The `File` menu contains two relevant commands: `New` and `Close`. The former will open a new window and the latter will close the current window.

## Directory Enumeration
The browse view is updated dynamically as files load (added in v1.1.0). The search field is disabled until file loading has ended but the `Recurse Subdirectories` checkbox remains enabled at all times.

## Caching
The FileBrowser app will remember the last directory path the user entered (in the initial directory selection dialog). The app will also refrain from reloading the same path, that is, if the the user hits `Esc` or clicks on `Cancel` from the directory browse window, the selection
dialog will be presented with the previously-entered path. If the user re-enters the same path, the app will not reload the files again. If the user really wants to refresh the view, issuing the `View->Refresh` command will accomplish this or simply toggling the `Recurse Subdirectories` checkbox will do the same.

## Browse View
All three columns (Name, Type, Size) in the browse view are sortable by clicking on their respective column headings.

## Recurse Subdirectories
Directory recursion is turned off by default. If the checkbox is toggled while directory enumeration is underway, the current process thread will be halted and restarted (with recursion enabled or disabled, whichever option has been selected). 

## Search Field
The current implementation of the search field restricts filtering to the `Name` property only.

## File Details Panel
The File Details Panel is implemented using _Cocoa Bindings_. All info is retrieved from the `MIXFile` object that is passed to an instance of the `FileDetailsPanelController` class when the user triggers the `Details` item on the context menu.

The panel does not update dynamically as the user moves from one row to the next using up and down arrow keys. However, the panel does update dynamically as soon as the user once again selects `Details` from the context menu. Obviously, there is room for improvement here.

## Requirements
This app was built on OSX 10.10.4 with Xcode 6.4. No third-party libraries were used. No fancy fiddling with the Xcode build has been undertaken.
