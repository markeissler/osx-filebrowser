//
//  MIXFileTests.m
//  FileBrowser
//
//  Created by Mark Eissler on 8/30/15.
//  Copyright (c) 2015 Mark Eissler. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "MIXFile.h"

@interface MIXFileTests : XCTestCase

@property (strong) MIXFile *sut;
@property (strong) NSString *path;
@property (strong) NSString *type;
@property (strong) NSString *prettyType;
@property (strong) NSURL *url;

@end

@implementation MIXFileTests

- (void)setUp
{
  [super setUp];
  // Put setup code here. This method is called before the invocation of each test method in the class.
  _path = @"/var/log/system.log";
  _url = [NSURL fileURLWithPath:_path];
  _sut = [[MIXFile alloc] initWithURL:_url];
}

- (void)tearDown
{
  // Put teardown code here. This method is called after the invocation of each test method in the class.
  _sut = nil;
  _url = nil;
  _path = nil;
  
  [super tearDown];
}

#pragma mark - Test init methods

- (void)testInitThrowsException
{
  XCTAssertThrowsSpecificNamed(
    [[MIXFile alloc] init], NSException,
    @"WrongInitializer",
    @"Init should throw a WrongInitializer exception");
}

- (void)testInitWithPathReturnsAnObject
{
  XCTAssertNotNil(
    [[MIXFile alloc] initWithPath:_path],
    @"Cannot create new object with initWithPath");
}

- (void)testInitWithUrlReturnsAnObject
{
  XCTAssertNotNil([[MIXFile alloc] initWithURL:self.url], @"Cannot create new object with initWithUrl");
}

#pragma mark - Test class methods

- (void)
  testClassMethodFileWithPathReturnsAnObject
{
  XCTAssertTrue(
    [MIXFile fileWithPath:self.path],
    @"Cannot create new obect with fileWithPath");
}

- (void)
  testClassMethodFileWithURLReturnsAnObject
{
  XCTAssertTrue(
    [MIXFile fileWithURL:self.url],
    @"Cannot create new object with fileWithURL");
}

#pragma mark - Test properties

- (void)testPropertyPathIsNotNil
{
  XCTAssertEqualObjects(self.sut.path,
    self.path,
    @"Property path cannot be set");
}

- (void)testPropertyUrlIsNotNil
{
  XCTAssertNotNil(self.sut.url, @"Property url is nil, it shouldn't be");
}

- (void)testPropertyNameIsNoNil
{
  XCTAssertNotNil(self.sut.name, @"Property name is nil, it shouldn't be");
}

- (void)testPropertyTypeIsNotNil
{
  XCTAssertNotNil(self.sut.type, @"Property type is nil, it shouldn't be");
}

- (void)testPropertyTypeIsCorrect
{
  NSString *type = self.sut.path.pathExtension;
  XCTAssertEqualObjects(self.sut.type, type, @"Property type is incorrect");
}

- (void)testPropertyPrettyTypeIsNotNil
{
  XCTAssertNotNil(self.sut.prettyType, @"Property prettyType is nil, it shouldn't be");
}

- (void)testPropertyPrettyTypeIsCorrect
{
  NSString *prettyType = @"Log File";
  XCTAssertEqualObjects(self.sut.prettyType, prettyType, @"Property prettyType is incorrect");
}

- (void)testPropertyIconIsNotNil
{
  XCTAssertNotNil(self.sut.icon, @"Property icon is nil, it shouldn't be");
}

- (void)testPropertyIconPathIsNil
{
  XCTAssertNil(self.sut.iconPath, @"Property iconPath is not nil, it should be");
}

- (void)testPropertySizeIsSet
{
  XCTAssertTrue(self.sut.size >= 0, @"Property size is not set");
}

- (void)testPropertyPrettySizeIsNotNil
{
  XCTAssertNotNil(self.sut.prettySize, @"Property prettySize is nil, it shouldn't be");
}

- (void)testPropertyPrettySizeIsValid
{
  BOOL foundUnit = NO;
  NSArray *units = @[
    @"bytes",
    @"KB",
    @"MB",
    @"GB",
    @"TB",
    @"PB",
    @"EB",
    @"ZB"
  ];
  
  NSString *prettySize = self.sut.prettySize;
  
  for (NSString *unit in units) {
    if ([prettySize hasSuffix:unit]) {
      foundUnit = YES;
      break;
    }
  }
  
  XCTAssertTrue(foundUnit, @"Property prettySize is invalid");
}

- (void)testPropertyCreatedIsNotNil
{
  XCTAssertNotNil(self.sut.created, @"Property created is nil, it shouldn't be");
}

- (void)testPropertyPrettyCreatedIsNotNil
{
  XCTAssertNotNil(self.sut.prettyCreated, @"Property prettyCreated is nil, it shouldn't be");
}

- (void)testPropertyPrettyCreatedIsValid
{
  XCTAssertTrue(self.sut.prettyCreated.length > 0, @"Property prettyCreated is zero length, it shouldn't be");
}

- (void)testPropertyModifiedIsNotNil
{
  XCTAssertNotNil(self.sut.modified, @"Property modified is nil, it shouldn't be");
}

- (void)testPropertyPrettyModifiedIsNotNil
{
  XCTAssertNotNil(self.sut.prettyModified, @"Property prettyModified is nil, it shouldn't be");
}

- (void)testPropertPrettyModifiedIsValid
{
  XCTAssertTrue(self.sut.prettyModified.length > 0, @"Property prettyModified is zero length, it shouldn't be");
}

#pragma mark - Test public methods

- (void)testPublicIsEqualToMIXFileReturnsTrueForSameObject
{
  XCTAssertTrue([self.sut isEqualToMIXFile:self.sut], @"Public method isEqualToMIXFile returned false for same object");
}

- (void)testPublicIsEqualToMIXFileReturnsFalseForDifferentObjects
{
  MIXFile *otherFile = [MIXFile fileWithPath:@"/etc/nfs.conf"];
  XCTAssertFalse([self.sut isEqualToMIXFile:otherFile], @"Public method isEqualToMIXFile returned true for different objects");
}

#pragma mark - Test NSCopying methods

- (void)testNSCopyingCopyWithZoneReturnsEqualObjects
{
  MIXFile *copyFile = [self.sut copy];
  XCTAssertTrue([self.sut isEqualToMIXFile:copyFile], @"NSCopying method copyWithZone returned unequal objects");
}

@end
