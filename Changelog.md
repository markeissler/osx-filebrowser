
v1.1.2 / 2016-07-31
===================

  * Fix missing ChangeLog entries.

v1.1.1 / 2016-07-31
===================

  * Update README for 1.1.1.
  * Reconfigure scheme to shared for jenkins-ci.
  * Fix incorrect asset size of icon_128x128.
  * Fix autolayout constraints.
  * Update project settings for Xcode 7.3.1.

v1.0.1 / 2015-08-31
===================

  * Add Changelog file.
  * Update README file.
  * Add unit tests for MIXFile model.
  * Fix MIXFile init to copy NSString objects. Add NSCopying protocol. Fix doc.
  * Code cleanup (removal of debug cruft).

v1.0.0 / 2015-08-15
===================

  * First release.

